const fsp = require("fs/promises");
const path = require("path");

function lipsum(filePath) {
  fsp
    .readFile(filePath, "utf-8")
    .then((content) => {
      console.log("lipsum.txt file Reading is Completed");
      return fsp.writeFile(path.join("upperCase.txt"), content.toUpperCase());
    })
    .then(() => {
      console.log("UpperCase.txt is writed");
      return fsp.appendFile(path.join("filenames.txt"), "upperCase.txt \n");
    })
    .then(() => {
      console.log("upperCase.txt file name is appended to filenames.txt");
      return fsp.readFile(path.join("upperCase.txt"), "utf-8");
    })
    .then((content) => {
      console.log("upperCase.txt file Reading is completed");
      let split = content.toLowerCase().split(".");
      return fsp.writeFile(path.join("lowerCase.txt"), JSON.stringify(split));
    })
    .then(() => {
      console.log("LowerCase.txt is writed ");
      return fsp.appendFile(path.join("filenames.txt"), "LowerCase.txt \n");
    })
    .then(() => {
      console.log("lowerCase.txt file name is appended to filenames.txt");
      return fsp.readFile(path.join("lowerCase.txt"), "utf-8");
    })
    .then((content) => {
      console.log("lowerCase.txt file Reading is completed");
      let dataSorted = JSON.parse(content).sort();
      return fsp.writeFile(path.join("sorted.txt"), JSON.stringify(dataSorted));
    })
    .then(() => {
      console.log("sorted.txt is writed");
      return fsp.appendFile(path.join("filenames.txt"), "sorted.txt \n");
    })
    .then(() => {
      console.log("sorted.txt filename is appended to filenames.txt");
      return fsp.unlink(path.join("upperCase.txt"));
    })
    .then(() => {
      console.log("upperCase.txt is deleted");
      return fsp.unlink(path.join("lowerCase.txt"));
    })
    .then(() => {
      console.log("lowerCase.txt is deleted");
      return fsp.unlink(path.join("sorted.txt"));
    })
    .then(() => {
      console.log("sorted.txt is deleted");
    })
    .catch((err) => {
      console.error(err);
    });
}
module.exports = lipsum;
// function lipsum(filePath) {
//   fs.readFile(filePath, "utf-8", (err, data) => {
//     if (err) {
//       console.error(err);
//     } else {
//       console.log("lipsum.txt file Reading is Completed");
//       let dataUpperCase = data.toUpperCase();
//       fs.writeFile("./upperCase.txt", dataUpperCase, (err) => {
//         if (err) {
//           console.error = err;
//         } else {
//           console.log("Uppercase.txt is Writed");
//           fs.appendFile("filenames.txt", "upperCase.txt", (err) => {
//             if (err) {
//               console.error(err);
//             } else {
//               console.log(
//                 "upperCase.txt file name is appended to filenames.txt"
//               );
//               fs.readFile("./upperCase.txt", "utf-8", (err, data) => {
//                 if (err) {
//                   console.error(err);
//                 } else {
//                   console.log("upperCase.txt file Reading is completed");
//                   let dataLowerCase = data.toLowerCase().split(".");
//                   fs.writeFile(
//                     "./lowerCase.txt",
//                     JSON.stringify(dataLowerCase),
//                     (err) => {
//                       if (err) {
//                         console.error = err;
//                       } else {
//                         console.log("LowerCase.txt is writed ");
//                         fs.appendFile(
//                           "filenames.txt",
//                           "lowerCase.txt",
//                           (err) => {
//                             if (err) {
//                               console.error(err);
//                             } else {
//                               console.log(
//                                 "lowerCase.txt file name is appended to filenames.txt"
//                               );
//                               fs.readFile(
//                                 "./lowerCase.txt",
//                                 "utf-8",
//                                 (err, data) => {
//                                   if (err) {
//                                     console.error(err);
//                                   } else {
//                                     console.log(
//                                       "lowerCase.txt file Reading is completed"
//                                     );
//                                     let dataSorted = JSON.parse(data).sort();
//                                     fs.writeFile(
//                                       "./sorted.txt",
//                                       JSON.stringify(dataSorted),
//                                       (err) => {
//                                         if (err) {
//                                           console.error = err;
//                                         } else {
//                                           console.log("sorted.txt is writed");
//                                           fs.appendFile(
//                                             "filenames.txt",
//                                             "sorted.txt",
//                                             (err) => {
//                                               if (err) {
//                                                 console.error(err);
//                                               } else {
//                                                 console.log(
//                                                   "sorted.txt filename is appended to filenames.txt"
//                                                 );
//                                                 fs.unlink(
//                                                   "./upperCase.txt",
//                                                   (err) => {
//                                                     if (err) {
//                                                       console.error(err);
//                                                     } else {
//                                                       console.log(
//                                                         "upperCase.txt is deleted"
//                                                       );
//                                                       fs.unlink(
//                                                         "./lowerCase.txt",
//                                                         (err) => {
//                                                           if (err) {
//                                                             console.error(err);
//                                                           } else {
//                                                             console.log(
//                                                               "lowerCase.txt is deleted"
//                                                             );
//                                                             fs.unlink(
//                                                               "./sorted.txt",
//                                                               (err) => {
//                                                                 if (err) {
//                                                                   console.error(
//                                                                     err
//                                                                   );
//                                                                 } else {
//                                                                   console.log(
//                                                                     "sorted.txt is deleted"
//                                                                   );
//                                                                 }
//                                                               }
//                                                             );
//                                                           }
//                                                         }
//                                                       );
//                                                     }
//                                                   }
//                                                 );
//                                               }
//                                             }
//                                           );
//                                         }
//                                       }
//                                     );
//                                   }
//                                 }
//                               );
//                             }
//                           }
//                         );
//                       }
//                     }
//                   );
//                 }
//               });
//             }
//           });
//         }
//       });
//     }
//   });
// }


