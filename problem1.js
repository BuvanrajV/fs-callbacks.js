const fsp = require("fs/promises");
const path = require("path");

function createFiles(folderPath, randomNum) {
  console.log(`Number of Files : ${randomNum}`);
  fsp
    .mkdir(folderPath, { recursive: true })
    .then(() => {
      console.log("Directory created");
      let arrAll = [];
      for (let random = 1; random <= randomNum; random++) {
        arrAll[random] = fsp.writeFile(
          path.join(`${folderPath}/file${random}.json`),
          "File creating"
        );
      }
      return Promise.all(arrAll);
    })
    .then(() => {
      console.log("All files are created");
      let arrAll = [];
      for (let random = 1; random <= randomNum; random++) {
        arrAll[random] = fsp.unlink(
          path.join(`${folderPath}/file${random}.json`)
        );
      }
      return Promise.all(arrAll);
    })
    .then(() => {
      console.log("All files are deleted");
    })
    .catch((err) => console.error(err));
}
module.exports = createFiles;

// function createFiles(folderPath, randomNum) {
//   fsp.mkdir(folderPath, () => {
//     console.log("directory created");
//     let fileCount = 0;
//     for (let random = 1; random <= randomNum; random++) {
//       fsp.writeFile(
//         path.join(`${folderPath}/file${random}.json`),
//         "File creating",
//         (err) => {
//           if (err) {
//             console.error(err);
//           } else {
//             fileCount += 1;
//             console.log(`file${random}.json created`);
//             if (fileCount === randomNum) {
//               console.log("Starting delete files");
//               for (let random = 1; random <= randomNum; random++) {
//                 fsp.unlink(path.join(`${folderPath}/file${random}.json`), (err) => {
//                   if (err) {
//                     console.error(err);
//                   } else {
//                     console.log(`file${random} Deleted`);
//                   }
//                 });
//               }
//             }
//           }
//         }
//       );
//     }
//   });
// }
